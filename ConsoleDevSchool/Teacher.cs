﻿using System;
using System.Linq;

namespace ConsoleDevSchool
{
    public class Teacher
    {
        public void TeacherOption()
        {
            // todo : LOGIN/PASSWORD
            

            Console.WriteLine("______________________________________________");
            Console.WriteLine("Укажите цифру что-бы выбрать свой факультет : " +
                              "\n1 - Faculty Of Computer Sciences" +
                              "\n2 - Faculty of Economics" +
                              "\n3 - Faculty of Psychology");
            Console.WriteLine("______________________________________________");

            var faculty = Console.ReadLine();
            switch (faculty)
            {
                case "1":
                    Console.WriteLine("______________________________________________" +
                                      "\n Ваш список студентов");
                    foreach (var studentList in UsersLists.StudentsList)
                    {
                        if (studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Dev"))
                        {
                            Console.WriteLine($"Name : {studentList.FirstName} {studentList.LastName}  |" +
                                              $"Student : {studentList.UserTypes == UserTypes.Student} |" +
                                              $"Dev : {studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Dev")} |" +
                                              $"");
                        }
                    }
                    break;

                case "2":
                    Console.WriteLine("______________________________________________" +
                                      "\n Ваш список студентов");
                    foreach (var studentList in UsersLists.StudentsList)
                    {
                        if (studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Designer"))
                        {
                            Console.WriteLine($"Name : {studentList.FirstName} {studentList.LastName}  |" +
                                              $"Student : {studentList.UserTypes == UserTypes.Student}  " +
                                              $"Designer : {studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Designer")}");
                        }
                    }
                    break;

                case "3":
                    Console.WriteLine("______________________________________________" +
                                      "\n Ваш список студентов");
                    foreach (var studentList in UsersLists.StudentsList)
                    {
                        if (studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Translator"))
                        {
                            Console.WriteLine($"Name : {studentList.FirstName} {studentList.LastName}  |" +
                                              $"Student : {studentList.UserTypes == UserTypes.Student} |" +
                                              $"Translator : {studentList.FacultyTypes == UsersLists.FacultyTypesList.FirstOrDefault(c => c.Name == "Translator")}");
                        }
                    }
                    break;
            }
        }
    }
}