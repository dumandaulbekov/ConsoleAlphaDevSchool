﻿namespace ConsoleDevSchool
{
    public enum UserTypes
    {
        Admin = 0,
        Teacher = 1,
        Student = 2
    }
}