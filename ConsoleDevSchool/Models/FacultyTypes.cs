﻿using System.Collections.Generic;

namespace ConsoleDevSchool
{
    public class FacultyTypes
    {
      
        public int id { get; set; }
        public string Name { get; set; }
        public List<SubjectTypes> Subjects { get; set; }
        public List<User> Users { get; set; }

    }
}