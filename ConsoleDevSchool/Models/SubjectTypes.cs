﻿using System.Collections.Generic;

namespace ConsoleDevSchool
{
    public class SubjectTypes
    {

        public int id { get; set; }
        public string Name { get; set; }
        public List<User> Student { get; set; }
        public List<User> Teacher { get; set; }
    }
}