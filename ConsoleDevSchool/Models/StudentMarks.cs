﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ConsoleDevSchool.Models
{
    public class StudentMarks
    {
        public int UserId { get; set; }
        public int Marks { get; set; }
        public int SubjectId { get; set; }
        
    }
}
