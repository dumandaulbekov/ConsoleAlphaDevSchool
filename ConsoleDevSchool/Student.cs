﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Xsl;

namespace ConsoleDevSchool
{
    public class Student
    {
        public void StudentOption()
        {
            bool again = false;
            while (!again)
            {
                Console.Write("Введите логин : ");
                var inputLogin = Console.ReadLine();
                var checkLogin = UsersLists.StudentsList.FirstOrDefault(c => c.Login == inputLogin);
                
                if ( checkLogin == null)
                {
                    Console.WriteLine("Win");

                }
                else
                {
                    Console.WriteLine("Введите заново");
                }

                Console.Write("Введите пороль : ");
                var inputPassword = Console.ReadLine();
                var checkPassword = UsersLists.StudentsList.FirstOrDefault(c => c.Password == inputPassword);

                if ( checkPassword == null)
                {
                    Console.WriteLine("Win");
                }
                else
                {
                    Console.WriteLine("Введите заново");
                }
            }
        }
    }
}