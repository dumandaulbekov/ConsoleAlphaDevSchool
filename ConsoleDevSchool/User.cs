﻿using ConsoleDevSchool.Models;

namespace ConsoleDevSchool
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public UserTypes UserTypes { get; set; }
        public FacultyTypes FacultyTypes { get; set; }
        public SubjectTypes SubjectTypes { get; set; }
        public StudentMarks StudentMarks { get; set; } 
    }
}