﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ConsoleDevSchool
{
    public class Admin
    {
        public static UsersLists UsersLists = new UsersLists();
        public void AdminOption()
        {
            Console.WriteLine("Функицонал Админа : " +
                              "\n1 - Показать всех Студентов " +
                              "\n2 - Показать всех Учителей " +
                              "\n2 - Добавить студента " +
                              "\n3 - Удалить стуендта ");

            var operationAdmin = Console.ReadLine();
            switch (operationAdmin)
            {
                case "1":
                    AllStudent();
                    break;
                case "2":
                    AllTeacher();
                    break;
                case "3":
                    AddUser();
                    break;
                case "4":
                    DeleteStudent();
                    break;
                default:
                    Console.WriteLine("Не правильный ввод ");
                    break;
            }

            void AllStudent()
            {
                Console.WriteLine("Все студенты школы имени AlphaDevSchool : ");
                
                foreach (var studentListAll in UsersLists.StudentsList)
                {
                    Console.WriteLine($"Id : {studentListAll.Id} " +
                                      $"| Name : {studentListAll.FirstName} {studentListAll.LastName} " +
                                      $"| Student : {studentListAll.UserTypes == UserTypes.Student} " +
                                      $"| FacultyType : {studentListAll.FacultyTypes} " +
                                      $"| Subject : {studentListAll.SubjectTypes} ");
                }
            }

            void AllTeacher()
            {
                Console.WriteLine("Все учителя школы имени AlphaDEvSchool : ");

                foreach (var teacherListAll in UsersLists.TeahersList)
                {
                    Console.WriteLine($"Id : {teacherListAll.Id}" +
                                      $"| Name : {teacherListAll.FirstName} {teacherListAll.LastName}" +
                                      $"| ");
                }
            }
            void AddUser()
            {
                int id = UsersLists.StudentsList.LastOrDefault().Id;

                Console.WriteLine("Введите Имя пользователя : ");
                string firstName = Console.ReadLine();

                Console.WriteLine("Введите Фамилию пользователя : ");

                string lastName = Console.ReadLine();

                Console.WriteLine("Введите Логин для пользователя : ");
                string login = Console.ReadLine();

                Console.WriteLine("Введите Пороль для пользователя : ");
                string password = Console.ReadLine();

                Console.WriteLine("Введите тип пользователя");
                UserTypes userTypes = (UserTypes) Enum.Parse(typeof(UserTypes), Console.ReadLine());

                Console.WriteLine("Введите тип факультета ");
                UsersLists.FacultyTypesList.Capacity.ToString();
               
                

                var newStudent = new User()
                    { Id = ++id,
                        FirstName = firstName, LastName = lastName,
                        Login = login, Password = password,
                        UserTypes = userTypes,

                        
                        
                    };

                UsersLists.StudentsList.Add(newStudent);
            }

            void DeleteStudent()
            {
                Console.WriteLine("Введите Id номер студента : ");
                var idStudent = Convert.ToInt32(Console.ReadLine());

                var student = UsersLists.StudentsList.FirstOrDefault(c => c.Id == idStudent);
                UsersLists.StudentsList.Remove(student);
            }
        }
    }
}