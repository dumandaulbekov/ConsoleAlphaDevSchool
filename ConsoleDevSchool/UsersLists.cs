﻿using System.Collections.Generic;

namespace ConsoleDevSchool
{
    public  class UsersLists
    {
        public static readonly List<User> StudentsList = new List<User>();
        public static readonly List<User> TeahersList = new List<User>();
        public static readonly List<SubjectTypes> SubjectTypesList = new List<SubjectTypes>();
        public static readonly List<FacultyTypes> FacultyTypesList = new List<FacultyTypes>();
        public static void FacultyTypesAvailableList()
        {
            FacultyTypesList.Add(new FacultyTypes() { id = 1, Name = "Dev", Users = TeahersList, Subjects = SubjectTypesList });
            FacultyTypesList.Add(new FacultyTypes() { id = 2, Name = "Designer", Users = TeahersList, Subjects = SubjectTypesList });
            FacultyTypesList.Add(new FacultyTypes() { id = 3, Name = "Translator", Users = TeahersList, Subjects = SubjectTypesList });
        }

        public static void SubjectTypesAvailableList()
        {
            SubjectTypesList.Add(new SubjectTypes() { id = 1, Name = "C#", Student = StudentsList, Teacher = TeahersList });
            SubjectTypesList.Add(new SubjectTypes() { id = 2, Name = "C++", Student = StudentsList, Teacher = TeahersList });

            SubjectTypesList.Add(new SubjectTypes() { id = 3, Name = "Architecture", Student = StudentsList, Teacher = TeahersList });
            SubjectTypesList.Add(new SubjectTypes() { id = 4, Name = "WebDesigner", Student = StudentsList, Teacher = TeahersList });

            SubjectTypesList.Add(new SubjectTypes() { id = 5, Name = "English", Student = StudentsList, Teacher = TeahersList });
            SubjectTypesList.Add(new SubjectTypes() { id = 6, Name = "Russian", Student = StudentsList, Teacher = TeahersList });
        }

        public static void StudentAvailableList()
        {
            StudentsList.Add(new User() { Id = 1, FirstName = "Bruce", LastName = "Wayne" , Login = "BW" , Password = "123"});
            StudentsList.Add(new User() { Id = 2, FirstName = "Harry", LastName = "Potter" , Login = "HP" , Password = "123 "});
            StudentsList.Add(new User() { Id = 3, FirstName = "Mike", LastName = "Vazovsky" });
            StudentsList.Add(new User() { Id = 4, FirstName = "Thomas", LastName = "Frank" });
            StudentsList.Add(new User() { Id = 5, FirstName = "Billy", LastName = "Batson" });
            StudentsList.Add(new User() { Id = 6, FirstName = "Quentin", LastName = "Tarantino" });
            StudentsList.Add(new User() { Id = 7, FirstName = "Naruto", LastName = "Uzumaki" });
            StudentsList.Add(new User() { Id = 8, FirstName = "Tom", LastName = "Hardy" });
        }
        public static void TeacherAvailableList()
        {
            TeahersList.Add(new User() { Id = 1, FirstName = "Mister", LastName = "Robot" });
            TeahersList.Add(new User() { Id = 2, FirstName = "Bags ", LastName = "Banny " });
            TeahersList.Add(new User() { Id = 3, FirstName = "Leo" , LastName = "Turtle" });
        }
    }
}