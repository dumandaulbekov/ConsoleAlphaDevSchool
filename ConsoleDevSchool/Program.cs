﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;

namespace ConsoleDevSchool
{
    public class Program
    {
       

        public static void Main(string[] args)
        {
            Student student = new Student();
            Teacher teacher = new Teacher();
            Admin admin = new Admin();

            bool repeatedOperation = true;
            while (repeatedOperation)
            {
                Console.WriteLine("Укажите цифру что-бы войти с определенного пользователя " +
                                  "\n1.Студент " +
                                  "\n2.Учитель ");
                Console.WriteLine("______________________________________________");

                var client = Console.ReadLine();
                switch (client)
                {
                    case "1":
                        student.StudentOption();
                        break;
                    case "2":
                        teacher.TeacherOption();
                        break;
                    case "3":
                        admin.AdminOption();
                        break;
                }
                Console.WriteLine("\n Нажмите Y Что-бы завершить операцию : ");
                var Continue = Console.ReadLine()?.ToLower();
                if (Continue == "Y")
                {
                    repeatedOperation = false;
                }
            }
        }
    }
}
